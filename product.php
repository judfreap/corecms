<?php include('header.php'); ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-gift"></i> Shop</h2>
                        <?php
                        if(isset($_SESSION['id']))
                        {
                            $ProductID = $_GET['id'];
                            if(empty($ProductID))
                            {
                                header("refresh:0; url=$custdir/404.php");
                            }
                            else
                            {
                                $bnetID = $_SESSION['id'];
                                if(empty($ProductID))
                                {
                                    header("refresh:0; url=$custdir/404.php");
                                }
                                else
                                {

                                    //let's search for the product just to be sure
                                    $pord_query = $mysqliA->query("SELECT * FROM `store_items` WHERE `id` = '$ProductID'") or die (mysqli_error($mysqliA));
                                    $num_query = $pord_query->num_rows;
                                    if($num_query < 1)
                                    {
                                        echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> Invalid product ID
                                        </div>
                                    ';
                                    }
                                    else
                                    {
                                        while($prod_res = $pord_query->fetch_assoc())
                                        {
                                            $ItemID = $prod_res['item_id'];
                                            //let's do the from
                                            echo '
                                        <div class="alert alert-info" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> You can\'t select online characters!
                                        </div>
                                        <form name="purchase" method="POST" action="'.$custdir.'/purchase.php">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="itemID" value="'. $ItemID .'">
                                                <input type="hidden" class="form-control" name="productID" value="'. $ProductID .'">
                                            </div>
                                            <div class="form-group">
                                                <select name="characterID" class="form-control" required>
                                                <option selected disabled>Select character</option>
                                        
                                        
                                        ';
                                            //let's get accid
                                            $acc_query = $mysqliA->query("SELECT * FROM `account` WHERE `battlenet_account` = '$bnetID';") or die (mysqli_error($mysqliA));
                                            while($acc_res = $acc_query->fetch_assoc())
                                            {
                                                $accountID = $acc_res['id'];
                                                //let's get characters of this account
                                                $get_chars_acc = $mysqliC->query("SELECT * FROM `characters` WHERE `account` = '$accountID';") or die (mysqli_error($mysqliC));
                                                $num_chars = $get_chars_acc->num_rows;
                                                if($num_chars < 1)
                                                {
                                                    echo '<option disabled="disabled">** No characters **</option>';
                                                }
                                                else
                                                {
                                                    while($char_res = $get_chars_acc->fetch_assoc())
                                                    {
                                                        $online = $char_res['online'];
                                                        $charID = $char_res['guid'];
                                                        $charName = $char_res['name'];
                                                        if($online < 1)
                                                        {
                                                            $status = '';
                                                        }
                                                        else
                                                        {
                                                            $status = 'disabled';
                                                        }
                                                        echo '<option value="'. $charID .'" '. $status .'>'. $charName .'</option>';
                                                    }
                                                }
                                            }
                                            echo '
                                            </select>
                                            </div>
                                            <button type="submit" name="purchase" class="btn btn-warning form-control"><i class="fad fa-box-heart"></i> Purchase</button>
                                        </form>
                                        ';
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            echo '
                                <div class="alert alert-warning" role="alert">
                                  <i class="fad fa-exclamation-circle"></i> You need to be <a href="'.$custdir.'/login.php">logged in</a> to access this page!
                                </div>
                            ';
                            header("refresh:3; url=$custdir/login.php");
                        }
                        ?>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>