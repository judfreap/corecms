<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-file-edit"></i> Item edit</div>
            <div class="card-body">
                <?php

                $done = 0;
                if(isset($_POST['additem']))
                {
                    $productID = stripslashes(mysqli_real_escape_string($mysqliA, $_GET['id']));
                    $itemid = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemid']));
                    $itemname = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemname']));
                    $itemcat = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['itemcat']));
                    $price = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['price']));

                    if(empty($itemid && $itemname) && $price == '')
                    {
                        $done = 1;
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Please check fields!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/store-items.php");
                    }
                    else
                    {
                        //insert
                        $item_update = $mysqliA->query("UPDATE `store_items` SET `item_id` = '$itemid', `item_name` = '$itemname', `price` = '$price', `category` = '$itemcat' WHERE `id` = '$productID';") or die (mysqli_error($mysqliA));
                        if($item_update === true)
                        {
                            $done = 1;
                            echo '
                                <div class="alert alert-success" role="alert">
                                  <i class="fad fa-check-circle"></i> Item was edited
                                </div>
                            ';
                            header("refresh:3; url=$custdir/acp/store-items.php");
                        }
                    }
                }
                else
                {
                    //get data from item already
                    $item_ID = stripslashes(mysqli_real_escape_string($mysqliA, $_GET['id']));
                    if(empty($item_ID))
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Invalid itemID 1
                            </div>
                        ';
                    }
                    else
                    {
                        //get data from store
                        $get_item = $mysqliA->query("SELECT * FROM `store_items` WHERE `id` = '$item_ID';") or die (mysqli_error($mysqliA));
                        $num_res = $get_item->num_rows;
                        if($num_res < 1)
                        {
                            echo '
                                <div class="alert alert-warning" role="alert">
                                  <i class="fad fa-exclamation-triangle"></i> Invalid itemID 2
                                </div>
                            ';
                        }
                        else
                        {
                            while($res = $get_item->fetch_assoc())
                            {
                                $editItemID = $res['item_id'];
                                $editItemName = $res['item_name'];
                                $editPrice = $res['price'];
                            }
                        }
                    }
                }
                if($done == 0)
                {
                ?>
                <form name="additem" method="post">
                    <div class="form-group">
                        <label for="itemid">ItemID</label>
                        <input type="text" name="itemid" class="form-control" value="<?php echo $editItemID ?>" required="required">
                        <small>You can get item id from wowhead</small>
                    </div>
                    <div class="form-group">
                        <label for="itemname">Item Name</label>
                        <input type="text" name="itemname" class="form-control"  value="<?php echo $editItemName ?>" required="required">
                        <small>Enter item name!</small>
                    </div>
                    <div class="form-group">
                    <label for="itemcat">Category</label>
                    <select name="itemcat" class="form-control">
                    <?php
                        $category_query = $mysqliA->query("SELECT * FROM `store_items_categorys`") or die (mysqli_error($mysqliA));
                        $num_query = $category_query->num_rows;
                        if($num_query < 1)
                            {
                                echo '<tr><td colspan="6">There are no Categorys!</td></tr>';
                            }
                        else
                        {
                            while($res = $category_query->fetch_assoc())
                            {
                                $categoryID = $res['id'];
                                $categoryName = $res['name'];

                                echo 
                                '
                                <option value="'.$categoryID.'">'.$categoryName.'</option>
                                ';
                            }
                        }
                    ?>
                    </select>
                    </div>
                    <div class="form-group">
                        <label for="price">Item Price</label>
                        <input type="text" name="price" class="form-control"  value="<?php echo $editPrice ?>" required="required">
                        <small>How many <span class="badge badge-warning"><i class="fad fa-coin"></i></span> (coins) this item will cost! </small>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success" name="additem"><i class="fad fa-file-edit"></i> Edit item</button>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>