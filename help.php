<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-life-ring"></i> Help!</h2>
                        <p class="card-text">
                        <div id="accordion">
                            <div class="card bg-transparent">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fad fa-question-circle"></i> Where i could get the <strong>World of Warcraft</strong> client ?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        You can get the <strong>Mini World of Warcraft</strong> client (500 mb) or you can get the <strong>Full World of Warcraft</strong> client (70 GB).<br />
                                        We recommend to install World of Warcraft on a SSD drive for better a gaming experience.<br />
                                        <hr />
                                        <a href="https://www.mediafire.com/file/xh57k96wm88ruy3/WOW_BFA_8.3.7.35662_ENUS_BFACORE_MINIMAL.exe" class="btn btn-outline-warning" target="_blank"><i class="fad fa-download"></i> Download Minimal Client</a> or <a href="https://www.mediafire.com/folder/t9ezftzfkx6w6/Battle_For_Azeroth_8.3.7.35662" class="btn btn-outline-warning" target="_blank"><i class="fad fa-download"></i> Download Full Client</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-transparent">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fad fa-question-circle"></i> How can i connect to the server ?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        To connect to our server you need a stable internet connection and an account which you can create it here on the website.<br />
                                        After you have created your account download the game or if you already have World of Warcraft installed please check if you have the right version of the game. (3.7.5 build 35662).<br />
                                        If you passed all the steps above edit your <strong>config.wtf</strong> file from <strong>WTF</strong> folder form your World of Warcraft install location and set the portal like this <code>SET PORTAL "<?php echo strtoupper($site_realm); ?>"</code>.<br />
                                        Now you can start the game and login with your email address and the password you set when you created the account.
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-transparent">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fad fa-question-circle"></i> How can i contact the server staff ?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        For any issues or problems please contact us on our <strong><a href="<?php echo $server_discord; ?>"><i class="fab fa-discord"></i> Discord Server</a> or send an email to <a href="mailto:<?php echo $site_contact; ?>"><?php echo $site_contact; ?></a></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-transparent">
                                <div class="card-header" id="headingFour">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <i class="fad fa-question-circle"></i> How can i buy items from the store ?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        You will need <span class="badge badge-warning"><i class="fad fa-coin"></i></span> coins. To buy coins go to your account and from there select the amount of coins you want to buy!
                                    </div>
                                </div>
                            </div>
                        </div>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>